<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Login-DDT Success</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>5</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>f8ce6135-b2f0-4009-ba7b-3cc32cb5ae61</testSuiteGuid>
   <testCaseLink>
      <guid>e3647828-64f5-445c-beaf-0b5ef2af6426</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC Manual/Scenario/Login/Login Positive Flow-DDT-TestSuite</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>16b7287c-e680-4dfa-9d46-e60bd0ec1177</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LoginData/LoginData-InternalData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>16b7287c-e680-4dfa-9d46-e60bd0ec1177</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>2014411a-4b3c-4c49-bbde-c48112101e75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>16b7287c-e680-4dfa-9d46-e60bd0ec1177</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>079f3ecc-1b77-4c88-852a-570f8b029419</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
